import React from 'react';
import { storiesOf } from '@storybook/react';
import { Button } from './Button';

//usamos el decorador importado para ver texto o color :
//import { text } from '@storybook/addon-knobs/react';

//definir historia del componente button tendra un modulo

storiesOf('Button', module)
//en esta instancia devolvera el button con bg= amarillo y help de children
//bg = es el background del componente button y yellow es el children
.add('with background', () => <Button bg="yellow"> Help </Button>)
//en estainstancia cambiaran las propiedades :
     //nombre con que se verá

.add('with background 2', () => <Button bg='green'> HelLO WORD </Button>);
//asi con kubs:                                //usamos text
//.add('with background 2', () => <Button bg={text{'bg','green'}}> HelLO WORD </Button>);
