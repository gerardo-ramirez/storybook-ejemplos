//para entender componentes react imortamos :
//importamos decorador:
//import { withKnobs } from '@storybook/addon-knobs/react';
//set Addon nos permite importar los modulos para  ver el codigo fuente x ej
//import { configure, setAddon, addDecorator} from '@storybook/react';

import { configure} from '@storybook/react';
//ver codigo:
//import JSXAddon from 'storybook-addon-jsx/register';
//setAddon(JSXAddon);
//utilizamos decorador:
//addDecorator(withKnobs);
//luego en el componente en ves de add usamos ADDwithJSX
//obtener todos los archivos .store


const req = require.context('../src',true, /.storybook.js$/)
function loadStories(){
  //configuramos el saludo de bienvenida:
  require('./welcomeStories')
  req.keys().forEach(file => req(file));
}

configure(loadStories, module);
